import {User, test, Expects} from '../index';

const user = new User({
    HOST: 'https://dog.ceo'
});

test('it should use internal expects', async t => {
    const result = await user.isAnon().GET(t, '/api/breeds/list/all')
        .expectStatus(200)
        .expectType({
            status: 'string',
            message: "object"
        })
        .json();

    Expects.expectType(t, result.message, {
        airedale: 'object',
        akita: 'object'
    });
});
