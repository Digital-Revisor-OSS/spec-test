import {mock, User, test} from '../index';

const mockClient = mock("mock", 2001);

const user = new User({
    HOST: mockClient.HOST
});

test('it should call the mock server', async t => {
    await mockClient.mockSimpleResponse('/my-endpoint', {success: true, message: "You did it!"}, 201);
    const result = await user.isAnon().GET(t, '/my-endpoint')
        .expectStatus(201)
        .expectType({
            "message": "string",
            "success": 'boolean'
        })
        .json();
    t.is(result.success, true)
    t.is(result.message, "You did it!");
});
