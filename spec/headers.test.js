import {mock, test, User} from '../index';

const mockClient = mock("mock", 2001);

const user = new User({
    HOST: mockClient.HOST
});

test('it should add headers to a request', async t => {
    await mockClient.mockAnyResponse({
        "httpRequest": {
            path: "/my/path",
            headers: {
                'X-My-Header': ['header value']
            }
        },
        "httpResponse": {
            "body": "OK"
        }
    });

    await user.isAnon().GET(t, '/my/path')
        .expectStatus(404)
        .text()

    await user.isAnon().GET(t, '/my/path', {'X-My-Header': 'header value'})
        .expectStatus(200)
        .expectText("OK")
        .text()
});
