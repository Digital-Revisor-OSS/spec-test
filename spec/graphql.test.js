import {Expects, mock, test, User} from '../index';

const mockClient = mock("mock", 2001);

const user = new User({
    HOST: mockClient.HOST
});

test('it should send a graphQL request', async t => {
    await mockClient.mockSimpleResponse('/graphql', {
        data: {
            getUserBy: {email: 'test@example.com', firstname: 'Test', lastname: 'Example'}
        }
    }, 200);
    const result = await user.isUser().GraphQL(t, `
                query($email: String!){
                    getUserBy(email: $email) {
                        email
                        firstname
                        lastname
                    }
                }
            `, {email: 'test@example.com'})
        .expectStatus(200)
        .expectType({
            data: 'object'
        })
        .json();
    Expects.expectType(t, result.data.getUserBy, {
        "email": "string",
        "firstname": "string",
        "lastname": "string"
    });
    t.is('Test', result.data.getUserBy.firstname)
});
