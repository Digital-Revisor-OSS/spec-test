import {User, test} from '../index';

const joe = new User({
    HOST: 'https://dog.ceo'
}).isUser({name: "Joe"});

const jane = new User({
    HOST: 'https://dog.ceo'
}).isUser({name: "Jane"});

test('it should use different users', async t => {
    const [joesResponse, janesResponse] = await Promise.all([
        joe.GET(t, '/api/breeds/list/all')
            .expectStatus(200)
            .json(),
        jane.GET(t, '/api/breeds/list/all')
            .expectStatus(200)
            .json()
    ]);
    t.is(joesResponse.status, janesResponse.status);
});
