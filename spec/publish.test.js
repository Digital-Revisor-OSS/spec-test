import {test, User} from '../index';

const user = new User({
    HOST: 'https://dog.ceo'
});

test('it should publish an event', async t => {

    let messagesReceived = 0;
    const waitFor = 3;

    const resultPromise = user.subscribe('test.subject', (msg) => {
        messagesReceived++;
    }, waitFor);

    await Promise.all([
        await user.isUser().publishEvent('test.subject', {
            eventName: 'event-1'
        }),
        await user.isUser().publishEvent('test.subject', {
            eventName: 'event-2'
        }),
        await user.isUser().publishEvent('test.subject', {
            eventName: 'event-3'
        })
    ]);

    t.is("SUCCESS", await resultPromise);

    t.is(true, messagesReceived >= 3, `got wrong number of messages: ${messagesReceived}, expected 3`);
});
