import {User, test} from '../index';

const user = new User({
    HOST: 'https://dog.ceo'
});

test('it should GET the json', t => {
    return user.isAnon().GET(t, '/api/breeds/list/all')
        .expectStatus(200)
        .json();
});
