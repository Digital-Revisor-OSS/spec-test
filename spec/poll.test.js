import {poll, test} from '../index';


test('it should poll until successful', async t => {
    // setup
    let counter = 0;
    const promiseGenerator = () => {
        // this could also be a user.isUser().GET(...) call
        return new Promise((resolve, reject) => {
            // do not resolve immediately, to have the temporal effect of a http call
            setTimeout(() => {
                counter++;
                resolve({counter: counter});
            }, 50);
        });
    };
    const validator = result => result.counter === 3;

    // execute
    const result = await poll(promiseGenerator, validator);

    // verify
    t.is(3, result.counter);

});
