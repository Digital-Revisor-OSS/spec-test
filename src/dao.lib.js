const mongodb = require('mongodb');
const fs = require('fs');

/**
 * @param {mongodb.Db} db 
 * @param {string} collectionName 
 * @param {string} fpath 
 */
const bootstrapCollection = (db, collectionName, fpath) => {
    return new Promise((resolve, reject) => {
        const collection = db.collection(collectionName);
        fs.readFile(fpath, 'utf-8', async (err, data) => {
            if (err) {
                reject(err);
            } else {
                await collection.insertMany(JSON.parse(data));
                resolve();
            }
        });
    });
};

/**
 * @param {string} connectionURL
 * @param {object} [options]
 * @returns {Promise<mongodb.Db>}
 */
const connect = (connectionURL, options = {}) => {
    return new Promise((resolve, reject) => {
        const client = new mongodb.MongoClient(connectionURL, {
            useNewUrlParser: true,
            ...options,
        });

        client.connect(err => {
            if (err) {
                reject(err);
            } else {
                resolve(client.db());
            }
        });
    });
};

module.exports = {
    connect,
    bootstrapCollection,
};