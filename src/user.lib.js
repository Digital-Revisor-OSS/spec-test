const jwt = require('jsonwebtoken'),
    Expects = require('./expects.lib'),
    fetch = require('node-fetch'),
    Events = require('./events.lib');

/*************************************
 *
 *    USER CLIENT
 *
 ************************************/

const DEFAULT_USERINFO = {
    tokenType: 'access',
    email: 'test@example.com',
    roles: [],
    id: '123452',
    displayName: 'Test Account',
    activeProducts: ['SC', 'VU', 'FK', 'RC'],
};

const DEFAULT_CONFIG = {
    EVENTS_LOCATION: process.env.EVENTS_LOCATION, 
    HOST: process.env.SUT,
    SECRET: 'SECRET',
};

function mergeCfg (cfg) {
    const out = {};
    for (let [ key, value ] of Object.entries(DEFAULT_CONFIG)) {
        if (cfg[key] !== undefined) {
            out[key] = cfg[key];
        } else {
            out[key] = value;
        }
    }
    return out;
}

class User {

    constructor(config = {}) {
        config = mergeCfg(config);
        this.SECRET = config.SECRET || 'SECRET';
        if (!config.HOST) {
            throw new Error('HOST not specified in config');
        }
        this.HOST = config.HOST;
        this.isUser();
        this.config = config;
    }

    setInfo(userInfo) {
        const info = Object.assign({}, DEFAULT_USERINFO, userInfo);
        this.jwt = jwt.sign(info, this.SECRET, {expiresIn: 60 * 60});
        this.info = info;
        return this;
    }

    makeToken(userInfo) {
        userInfo = userInfo || {};
        const info = Object.assign({}, DEFAULT_USERINFO, userInfo);
        return jwt.sign(info, this.SECRET, {expiresIn: 60 * 60});
    }

    isUser(info) {
        return this.setInfo(info ? info : {});
    }

    isAdmin() {
        return this.setInfo({roles: ['admin']});
    }

    isToken(token) {
        this.info = jwt.decode(token);
        this.jwt = token;
        return this;
    }

    isAnon() {
        this.jwt = null;
        this.info = null;
        return this;
    }

    GET(t, path, headers) {
        return this._http(t, 'GET', path, {
            headers: headers
        });
    }

    PUT(t, path, data, headers) {
        return this._http(t, 'PUT', path, {
            json: data,
            headers: headers
        });
    }

    PATCH(t, path, data, headers) {
        return this._http(t, 'PATCH', path, {
            json: data,
            headers: headers
        });
    }

    POST(t, path, data, headers) {
        return this._http(t, 'POST', path, {
            json: data,
            headers: headers
        });
    }

    DELETE(t, path, data, headers) {
        return this._http(t, 'DELETE', path, {
            json: data,
            headers: headers
        });
    }

    POSTFILE(t, path, file, contentType, fileName) {
        // TODO
        return this._http(t, 'POST', path, {
            file: file,
            contentType: contentType
        });
    }

    publishEvent(subject, event) {
        if (!this.events) {
            this.events = new Events(this.config)
        }
        return this.events.publish(subject, event)
    }

    subscribe(subject, cb, waitFor) {
        if (!this.events) {
            this.events = new Events(this.config)
        }
        return this.events.subscribe({ subject, handler: cb, waitFor });
    }

    GraphQL(t, query, variables, headers) {
        return this._http(t, 'POST', '/graphql', {
            json: {query, variables},
            headers: headers
        })
    }

    _http(t, method, path, cfg) {
        // setup
        const url = `${this.HOST}${path}`;
        const init = {
            method: method,
            headers: {},
            mode: 'cors',
            cache: 'default',
            credentials: 'include'
        };
        if (cfg && cfg.json) {
            init['body'] = JSON.stringify(cfg.json);
            init['headers']['Content-Type'] = 'application/json';
            init['headers']['Accept'] = 'application/json';
        }
        if (cfg && cfg.file) {
            init['body'] = cfg.file;
            init['headers']['Content-Type'] = cfg.contentType;
            init['headers']['Accept'] = 'application/json';
        }
        if (cfg && cfg.headers) {
            for (const key in cfg.headers) {
                if (cfg.headers.hasOwnProperty(key)) {
                    init['headers'][key] = cfg.headers[key];
                }
            }
        }

        if (this.jwt) {
            init['headers']['Authorization'] = `Bearer ${this.jwt}`
        }

        // execute
        const requestPromise = fetch(`${url}`, init);
        return new ExpectBuilder(t, requestPromise);
    }

}

class ExpectBuilder {

    constructor(test, response) {
        this.response = response;
        this._expectations = {};
        this.test = test;
    }

    // Lazy load the http body as json
    json() {
        if (!this._json) {
            this._json = new Promise((resolve, reject) => {
                this.response.then(result => {
                    result.json().then(resolve, reject);
                });
            });
        }
        return this._json;
    }

    // Lazy load the http body as text
    text() {
        if (!this._text) {
            this._text = new Promise((resolve, reject) => {
                this.response.then(result => {
                    result.text().then(resolve, reject);
                });
            });
        }
        return this._text;
    }

    expectText(expectedText) {
        this._expectations.text = expectedText;
        this.text().then(text => Expects.expectText(this.test, text, expectedText));
        return this;
    }

    expectStatus(expectedCode) {
        this._expectations.status = expectedCode;
        this.response.then(result => Expects.expectStatus(this.test, result, expectedCode));
        return this;
    }

    expectHeader(header, value) {
        this._expectations.header = {header, value};
        this.response.then(result => Expects.expectHeader(this.test, result, header, value));
        return this;
    }

    expectJson(expectedData) {
        this._expectations.json = expectedData;
        this.json().then(json => Expects.expectJson(this.test, json, expectedData));
        return this;
    }

    expectType(types) {
        this._expectations.types = types;
        this.json().then(json => Expects.expectType(this.test, json, types));
        return this;
    }

    logJson() {
        this.json().then(json => {
            console.log(JSON.stringify(json, null, 2));
        });
        return this;
    }

}

module.exports = User;
