const { mockServerClient } = require('mockserver-client');
const User = require('./user.lib');

// looks at all entries in the provided object recursivly
// and remove the ones where the entry value is undefined
function deleteUndefinedProps (object) {
    for (let [key, value] of Object.entries(object)) {
        if (typeof value === 'object' && value !== null) {
            deleteUndefinedProps(value);
        } else if (value === undefined) {
            delete object[key];
        }
    }

    return object;
}

/**
 * @typedef {object} mockOptions
 * @property {string} [method]
 * @property {string} [path]
 * @property {number} [statusCode]
 * @property {object} [response]
 * @property {object} [headers]
 */

/**
 * @param {mockOptions} props
 * @param {User} [user]
 */
const mockEndpoint = (props, user) => {
    // prepare request body
    let body;
    if (props.response) {
        body = JSON.stringify(props.response);
    }

    let headers = props.headers || {};

    // if user provided, only match
    // requests by the provided user
    if (user) {
        // match the authorization header with the users JWT token
        headers = {
            ...headers,
            authorization: [`Bearer ${user.jwt}`],
        };
    }

    // formats the data to be received by
    // a "mockAnyResponse" call on a
    // mock server client instance
    return deleteUndefinedProps({
        httpRequest: {
            method: props.method,
            path: props.path,
            headers,
        },
        httpResponse: {
            statusCode: props.statusCode || 200,
            body,
        },
    });
};

/**
 * Mocks an endpoint used for internal communication.
 * Formatted like so:
 * {
 *   "success": boolean,
 *   "message": string,
 *   "data": any
 * }
 * @param {mockOptions} options 
 * @param {boolean} success 
 * @param {User} [user]
 */
const mockInternalEndpoint = (options, success, user) => {
    const { response, ...other } = options;
    return mockEndpoint({
        ...other,
        response: {
            success,
            data: response,
        }
    }, user);
};

/**
 * @param {mockOptions} options 
 * @param {User} [user]
 */
const mockSuccess = (options, user) => mockInternalEndpoint(options, true, user);

/**
 * @param {mockOptions} options 
 * @param {User} [user]
 */
const mockFailure = (options, user) => mockInternalEndpoint(options, false, user);

/**
 * 
 * @param {*} client 
 * @param {*} handler 
 * @returns {(options: mockOptions, user?: User) => any}
 */
const anyResponseWrapper = (client, handler) => {
    return (options, user) => {
        return client.mockAnyResponse([handler(options, user)]);
    };
};

function mock(service, port) {
    const { DOCKER_MACHINE_NAME, CI } = process.env;
    
    let usedService;
    
    // If CI is set, use defined service name instead of localhost
    if (CI) {
        usedService = service;
    } else {
        // If DOCKER_MACHINE_NAME is set, use that instead of localhost
        if (DOCKER_MACHINE_NAME) {
            usedService = DOCKER_MACHINE_NAME;
        } else {
            usedService = 'localhost';
        }
    }

    const client = mockServerClient(usedService, port);
    client.HOST = `http://${usedService}:${port}`;

    // Inject customer methods into the client for convenience
    const clientWrapper =  {
        mockEndpoint: anyResponseWrapper(client, mockEndpoint),
        mockSuccess: anyResponseWrapper(client, mockSuccess),
        mockFailure: anyResponseWrapper(client, mockFailure),
        ...client,
    };

    return clientWrapper;
}

module.exports = mock;