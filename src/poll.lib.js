/**
 * A polling mechanism that have practical use when doing async communication.
 *
 * See poll.test.js for an example.
 *
 * The poll have a 10 second timeout at which point it will return null. Otherwise it
 * returns the successfully validated result of the promiseGenerator.
 *
 * @param promiseGenerator function that returns a promise
 * @param validator function that returns true or false if the input is considered ready.
 * @returns {Promise<any>}
 */
function poll(promiseGenerator, validator) {
    const timeout = 10000;
    let deadline = new Date().getTime() + timeout;

    return new Promise((resolve, reject) => {
        const pollFn = async () => {
            if (new Date().getTime() > deadline) {
                reject(null);
            }
            const result = await promiseGenerator()
            if (validator(result)) {
                resolve(result);
            }
            setTimeout(pollFn, 100);
        };
        return pollFn();
    });
}

module.exports = poll;