class Expects {

    static expectType(t, json, types) {
        for (const [field, expectedType] of Object.entries(types)) {
            const receivedType = typeof json[field];
            t.is(receivedType, expectedType, `wrong type of field '${field}'`);
        }
        return this;
    }

    static expectJson(t, json, expectedData) {
        t.deepEqual(json, expectedData, "unexpected json returned");
        return this;
    }

    static expectHeader(t, result, header, value) {
        const receivedValue = result.headers.get(header);
        t.is(receivedValue, value, `wrong header '${header}'`);
        return this;
    }

    static expectStatus(t, result, expectedCode) {
        t.is(result.status, expectedCode, "unexpected http status code");
        return this;
    }

    static expectText(t, text, expectedText) {
        t.is(text, expectedText, "unexpected text returned");
        return this;
    }

}

module.exports = Expects;