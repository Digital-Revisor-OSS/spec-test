const nats = require('node-nats-streaming');

class Events {
    constructor({ EVENTS_LOCATION, CLUSTER, CLIENTID }) {
        this.EVENTS_LOCATION = EVENTS_LOCATION || 'nats://localhost:4222';
        this.CLUSTER = CLUSTER || 'events';
        this.CLIENTID = CLIENTID || 'test-clist';
        this._conn = null;
    }

    /**
     * @returns {Promise<nats.Stan}
     */
    connect() {
        if (!this._conn) {
            this._conn = new Promise((resolve, reject) => {
                try {
                    this.stan = nats.connect(this.CLUSTER, this.CLIENTID, this.EVENTS_LOCATION);
                } catch (e) {
                    reject(e);
                }
                this.stan.on('connect', () => {
                    resolve(this.stan);
                });
            })
        }
        return this._conn;
    }

    async publish(subject, event) {
        const stan = await this.connect();
        return new Promise((resolve, reject) => {
            let eventMsg = event;
            if (typeof event === 'object') {
                eventMsg = JSON.stringify(event);
            }
            stan.publish(subject, eventMsg, function (err, guid) {
                if (err) {
                    reject(err);
                } else {
                    resolve(guid);
                }
            });

        });
    }

    async subscribe({
        subject,
        handler,
        waitFor = 2,
        timeout = 3000,
        startTime = new Date(),
        durableName = null,
    }) {
        if (!subject) {
            throw new Error('"subject" is required');
        }

        let receiveCount = 0;

        const stan = await this.connect();

        // prepare subscription options
        let opts;
        opts = stan.subscriptionOptions();
        opts = opts.setStartTime(startTime);
        if (durableName) {
            opts = opts.setDurableName(durableName);
        }

        const subscription = stan.subscribe(subject, opts);
        subscription.on('message', (msg) => {
            receiveCount++;
            handler && handler(msg);
        });

        return Promise.race([
            new Promise(resolve => {
                setTimeout(resolve, timeout, 'TIMEOUT');
            }),
            new Promise(resolve => {
                const pollFn = () => {
                    if (receiveCount >= waitFor) {
                        resolve("SUCCESS");
                    } else {
                        setTimeout(pollFn, 100);
                    }
                };
                pollFn();
            })
        ]);
    }
}


module.exports = Events;