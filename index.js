module.exports = {
    User: require('./src/user.lib'),
    Events: require('./src/events.lib'),
    Expects: require('./src/expects.lib'),
    mock: require('./src/mock.lib'),
    poll: require('./src/poll.lib'),
    dao: require('./src/dao.lib'),
    test: require('ava'),
    mongodb: require('mongodb'),
};