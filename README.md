# Spec Test utilities

This package helps create HTTP based tests against a local HTTP server.

It can be used to quickly try out the behaviour of a HTTP server during development.

## Downloading

Add the following to your `package.json`:

`
  "devDependencies": {
    "spec-test": "gitlab:Digital-Revisor-OSS/spec-test"
  }
`

Or use:

`npm i gitlab:Digital-Revisor-OSS/spec-test`

## Links

The module uses AVA to do test assertions. Read up on the syntax on the AVA [Github Page](https://github.com/avajs/ava)

For mocking a backend, read how to use the [MockServer](http://www.mock-server.com/mock_server/mockserver_clients.html).

## Example: Anonymous HTTP call

```javascript
import {User, test} from 'spec-test';

const user = new User({
    HOST: 'https://dog.ceo'
});

test('it should GET the json', t => {
    return user.isAnon().GET(t, '/api/breeds/list/all')
        .expectStatus(200)
        .json();
});

```

## Example: Multiple Users

It is possible to have different users and have their requests be made in parallel:

```javascript
import {User, test} from 'spec-test';

const joe = new User({
    HOST: 'https://dog.ceo'
}).isUser({name: "Joe"});

const jane = new User({
    HOST: 'https://dog.ceo'
}).isUser({name: "Jane"});

test('it should use different users', async t => {
    const [joesResponse, janesResponse] = await Promise.all([
        joe.GET(t, '/api/breeds/list/all')
            .expectStatus(200)
            .json(),
        jane.GET(t, '/api/breeds/list/all')
            .expectStatus(200)
            .json()
    ]);
    t.is(joesResponse.status, janesResponse.status);
});

```

## Example: Using a mock backend

Sometimes the service you need to test calls a deputy service. it's convenient to also mock that service and instrument it. This can be done like this:
In this case, we call the mocked deputy service directly, but in real-life code, you would call your service under test.

```javascript
import {mock, User, test} from '../index';

const mockClient = mock("mock", 2001);

const user = new User({
    HOST: mockClient.HOST
});

test('it should call the mock server', async t => {
    await mockClient.mockSimpleResponse('/my-endpoint', {success: true, message: "You did it!"}, 201);
    const result = await user.isAnon().GET(t, '/my-endpoint')
        .expectStatus(201)
        .expectType({
            "message": "string",
            "success": 'boolean'
        })
        .json();
    t.is(result.success, true)
    t.is(result.message, "You did it!");
});

````

## Example: Performing Expect check on subfields

Sometimes a services returns lists of objects that would be convenient to type check - or just type check sub fields.
This can be done like this:

````javascript
import {User, test, Expects} from '../index';

const user = new User({
    HOST: 'https://dog.ceo'
});

test('it should use internal expects', async t => {
    const result = await user.isAnon().GET(t, '/api/breeds/list/all')
        .expectStatus(200)
        .expectType({
            status: 'string',
            message: "object"
        })
        .json();

    Expects.expectType(t, result.message, {
        airedale: 'object',
        akita: 'object'
    });
});


````

## Example: Add Headers to http requests

Sometimes it's useful to add HTTP Headers to the request. This is done by adding a last argument which contains the headers.

````javascript

import {mock, test, User} from '../index';

const mockClient = mock("mock", 2001);

const user = new User({
    HOST: mockClient.HOST
});

test('it should add headers to a request', async t => {
    await mockClient.mockAnyResponse({
        "httpRequest": {
            path: "/my/path",
            headers: {
                'X-My-Header': ['header value']
            }
        },
        "httpResponse": {
            "body": "OK"
        }
    });

    await user.isAnon().GET(t, '/my/path')
        .expectStatus(404)
        .text()

    await user.isAnon().GET(t, '/my/path', {'X-My-Header': 'header value'})
        .expectStatus(200)
        .expectText("OK")
        .text()
});

````

## Example: Using GraphQL

If required to use GraphQL the library have convenience methods to do that against the `/graphql` url of the system under test

It can be used like this:

````javascript
import {Expects, mock, test, User} from '../index';

const mockClient = mock("mock", 2001);

const user = new User({
    HOST: mockClient.HOST
});

test('it should send a graphQL request', async t => {
    await mockClient.mockSimpleResponse('/graphql', {
        data: {
            getUserBy: {email: 'test@example.com', firstname: 'Test', lastname: 'Example'}
        }
    }, 200);
    const result = await user.isUser().GraphQL(t, `
                query($email: String!){
                    getUserBy(email: $email) {
                        email
                        firstname
                        lastname
                    }
                }
            `, {email: 'test@example.com'})
        .expectStatus(200)
        .expectType({
            data: 'object'
        })
        .json();
    Expects.expectType(t, result.data.getUserBy, {
        "email": "string",
        "firstname": "string",
        "lastname": "string"
    });
    t.is('Test', result.data.getUserBy.firstname)
});
````

## Example: Polling

For convenience a polling method is added. It can be used to repeat a http call until a value changes in the result.

````javascript
import {poll, test} from '../index';

test('it should poll until successful', async t => {
    // setup
    let counter = 0;
    const promiseGenerator = () => {
        // this could also be a user.isUser().GET(...) call
        return new Promise((resolve, reject) => {
            // do not resolve immediately, to have the temporal effect of a http call
            setTimeout(() => {
                counter++;
                resolve({counter: counter});
            }, 50);
        });
    };
    const validator = result => result.counter === 3;

    // execute
    const result = await poll(promiseGenerator, validator);

    // verify
    t.is(3, result.counter);

});

````
